import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product:any[];
  prodId:any

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getProduct(this.route.snapshot.params['id']).subscribe((dataFromBack) => {
      console.log(dataFromBack);
      this.product = dataFromBack.data;
      this.prodId= dataFromBack.data.id;
    });
  }

  EditZone(){

    this.router.navigate(['product-edit/'+this.prodId]);
  }

}