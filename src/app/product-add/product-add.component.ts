import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  @Input() NewProductData = { prod_name:'', prod_desc: '', prod_price: 0 };

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  addProduct() {
    this.rest.addProduct(this.NewProductData).subscribe((dataFromBack) => {
      console.log(dataFromBack);
      console.log(dataFromBack.data);
      this.router.navigate(['product-detail/'+dataFromBack.data.id]);
    }, (err) => {
      console.log(err);
    });
  }

}