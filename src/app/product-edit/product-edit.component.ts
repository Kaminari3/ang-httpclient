import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  @Input() productData:any = { prod_name: '', prod_desc: '', prod_price:0 };
  resultID:any;

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getProduct(this.route.snapshot.params['id']).subscribe((dataFromBack) => {
      console.log(dataFromBack);
      this.productData = dataFromBack.data;
    });
  }

  updateProduct() {
    this.rest.updateProduct(this.route.snapshot.params['id'], this.productData).subscribe((result) => {
      this.resultID=result.data;
      this.router.navigate(['product-detail/'+this.resultID.id]);
    }, (err) => {
      console.log(err);
    });
  }

}